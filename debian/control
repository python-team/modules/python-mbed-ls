Source: python-mbed-ls
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 doxygen,
 pandoc,
 python3-all,
 python3-colorlog,
 python3-fasteners,
 python3-platformdirs,
 python3-prettytable,
 python3-pytest,
 python3-setuptools,
Standards-Version: 4.6.2
Homepage: https://github.com/ARMmbed/mbed-ls
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-mbed-ls
Vcs-Git: https://salsa.debian.org/python-team/packages/python-mbed-ls.git
Rules-Requires-Root: no

Package: python3-mbed-ls
Architecture: all
Depends:
 python3-colorlog,
 python3-pkg-resources,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-mbed-ls-doc,
Description: module listing mbed-enabled devices connected to host (Python 3)
 This module detects and lists "Mbed Enabled" devices connected to the
 host computer.
 .
 mbedls provides the following information for all connected boards using
 console (terminal) output:
 .
  -  Mbed OS platform name
  -  mount point (MSD or disk)
  -  serial port
 .
 This package contains the module for Python 3 and the mbedls utility.

Package: python-mbed-ls-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-jquery,
 ${misc:Depends},
Enhances: python3-mbed-ls
Breaks: python3-mbed-ls-doc
Replaces: python3-mbed-ls-doc
Description: mbed-ls library documentation
 This module detects and lists "Mbed Enabled" devices connected to the
 host computer.
 .
 mbedls provides the following information for all connected boards using
 console (terminal) output:
 .
  -  Mbed OS platform name
  -  mount point (MSD or disk)
  -  serial port
 .
 This package contains library documentation.
