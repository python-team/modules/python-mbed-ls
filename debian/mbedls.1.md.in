% MBEDLS(1) __VERSION__ | User Commands
% 
% December 2018

# NAME

**mbedls** -  detect and list Mbed Enabled devices connected to the host computer

# SYNOPSIS

| **mbedls** \[**\-\-simple**|**\-\-json**\] \[**\-\-debug**\]

| **mbedls** **\-\-list**

| **mbedls** **\-\-mock** _ID:NAME_

| **mbedls** **\-\-help**

| **mbedls** **\-\-version**

# DESCRIPTION

**mbedls** detects and lists ARM Mbed Enabled devices connected to the host
computer.

It provides the following information for all connected boards in a console
(terminal) output:

- Mbed OS platform name
- mount point (MSD or disk)
- serial port

**mbedls** requires that an Mbed device be mounted before it shows up in the results.
Many systems do not automatically mount USB devices. It is recommended that
you use an automounter to manage this for you.

# OPTIONS

-s, \-\-simple
:   list attached targets without column headers and borders

-j, \-\-json
:   list attached targets with detailed information in JSON format

-J, \-\-json-by-target-id
:   map attached targets from their target ID to their detailed information in JSON format

-p, \-\-json-platforms
:   list attached platform names in JSON format

-P, \-\-json-platforms-ext
:   map attached platform names to the number of attached boards in JSON format

-l, \-\-list
:   list all target IDs and their corresponding platform names understood by **mbedls**

-m, \-\-mock _ID:NAME_
:   substitute or create a target ID to platform name mapping used when invoking **mbedls** in the current directory

\-\-skip-retarget
:   skip parsing and interpretation of the re-target file, `./mbedls.json`

-u, \-\-list-unmounted
:   list mbeds, regardless of whether they are mounted or not

-d, \-\-debug
:   outputs extra debug information useful when creating issues!

\-\-help
:   print usage details and exit

\-\-version
:   print package version and exit

# RESULT FORMATS

The **mbedls** command-line accepts a few arguments to change the format of the results. The default format is a table. You may pass `--simple` to simplify this table format, and `--json` to print the table as a JSON list of the rows.

## Simple (no table formatting)

```
$ mbedls --simple
 K64F  K64F[0]  D:  COM18  0240000032044e4500257009997b00386781000097969900  0244
```

## JSON

```bash
$ mbedls --json
[
    {
        "daplink_auto_reset": "0",
        "daplink_automation_allowed": "1",
        "daplink_bootloader_crc": "0xa65218eb",
        "daplink_bootloader_version": "0242",
        "daplink_daplink_mode": "Interface",
        "daplink_git_sha": "67f8727a030bcc585e982d899fb6382db56d673b",
        "daplink_hic_id": "97969900",
        "daplink_interface_crc": "0xe4422294",
        "daplink_interface_version": "0244",
        "daplink_local_mods": "0",
        "daplink_overflow_detection": "1",
        "daplink_remount_count": "0",
        "daplink_unique_id": "0240000032044e4500257009997b00386781000097969900",
        "daplink_usb_interfaces": "MSD, CDC, HID",
        "daplink_version": "0244",
        "mount_point": "D:",
        "platform_name": "K64F",
        "platform_name_unique": "K64F[0]",
        "product_id": "0204",
        "serial_port": "COM18",
        "target_id": "0240000032044e4500257009997b00386781000097969900",
        "target_id_mbed_htm": "0240000032044e4500257009997b00386781000097969900",
        "target_id_usb_id": "0240000032044e4500257009997b00386781000097969900",
        "vendor_id": "0d28"
    }
]
```

# MOCKING (RENAMING) PLATFORMS

Override a platform's name using the `--mock` parameter:

```
$ mbedls --mock 0240:MY_NEW_PLATFORM
$ mbedls
+-----------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| platform_name   | platform_name_unique | mount_point | serial_port | target_id                                        | daplink_version |
+-----------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| MY_NEW_PLATFORM | MY_NEW_PLATFORM[0]   | D:          | COM18       | 0240000032044e4500257009997b00386781000097969900 | 0244            |
+-----------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
```

The `--mock` parameter accepts a platform ID and a platform name, separated by the `:` character. The platform ID is the first 4 characters of the `target_id`. The platform name is the name you are temporarily assigning to this platform.

To remove a mocked platform, use the `--mock` parameter again. Continuing from the previous example, use `-<platform id>` as the value:

```
$ mbedls --mock -0240
$ mbedls
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| platform_name | platform_name_unique | mount_point | serial_port | target_id                                        | daplink_version |
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| K64F          | K64F[0]              | D:          | COM18       | 0240000032044e4500257009997b00386781000097969900 | 0244            |
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
```

You can also remove all mocked platforms by supplying `*` as the `target_id`:

```
$ mbedls --mock="-*"
```

**NOTE:** Due to a quirk in the parameter formatting, the command-line can interpret `-*` as another parameter instead of a value. It is necessary to use the complete `--mock="-*"` syntax, so the command-line interprets each part of the command correctly.

# RETARGETTING PLATFORMS

It is possible to change the returned results for certain platforms depending on the current directory. This is especially useful when developing new platforms.

The command-line tool and Python API check the current directory for a file named `mbedls.json`. When it is present, it overrides the returned values. The format of the `mbedls.json` file is:

```json
{
    "<target_id>": {
        "<key>": "<value>"
    }
}
```

For example, to change the `serial_port` of the K64F with a `target_id` of `0240000032044e4500257009997b00386781000097969900`, the `mbedls.json` file contains the following:

```json
{
    "0240000032044e4500257009997b00386781000097969900": {
        "serial_port": "COM99"
    }
}
```

This results in the following output from the command-line tool:

```bash
$ mbedls
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| platform_name | platform_name_unique | mount_point | serial_port | target_id                                        | daplink_version |
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
| K64F          | K64F[0]              | D:          | COM99       | 0240000032044e4500257009997b00386781000097969900 | 0244            |
+---------------+----------------------+-------------+-------------+--------------------------------------------------+-----------------+
```

Note how the `serial_port` value changed from `COM18` to `COM99`. Deleting the `mbedls.json` or using the `--skip-retarget` parameter removes these changes.

# FILES

*mbedls.json*

:   Per-user re-target file

# REPORTING BUGS

Upstream bug tracker: https://github.com/ARMmbed/mbed-ls/issues

# COPYRIGHT

Copyright (c) 2011-2015 ARM Limited

# AUTHOR

This manual page is based on the mbedls documentation. It was created by Nick Morrott <nickm@debian.org> for the Debian GNU/Linux system, but may be used by others

# SEE ALSO

**yotta**(1), **valinor**(1)
